/* This file is part of Ping903
   Copyright (C) 2020-2025 Sergey Poznyakoff
  
   Ping903 is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
  
   Ping903 is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
  
   You should have received a copy of the GNU General Public License
   along with Ping903.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <config.h>
#include <stdlib.h>
#include <string.h>
#include "defs.h"
#include "json.h"

void
emalloc_die(void)
{
	fatal("not enough memory");
	exit(2);
}

void *
emalloc(size_t s)
{
	void *p = malloc(s);
	if (!p)
		emalloc_die();
	return p;
}

void *
ecalloc(size_t n, size_t s)
{
	void *p = calloc(n, s);
	if (!p)
		emalloc_die();
	return p;
}

void *
e2nrealloc(void *p, size_t *pn, size_t s)
{
	char *n = json_2nrealloc(p, pn, s);
	if (!n)
		emalloc_die();
	return n;
}

char *
estrdup(char const *s)
{
	return strcpy(emalloc(strlen(s) + 1), s);
}
