/* This file is part of Ping903
   Copyright (C) 2020-2025 Sergey Poznyakoff
  
   Ping903 is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
  
   Ping903 is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
  
   You should have received a copy of the GNU General Public License
   along with Ping903.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <pthread.h>
#include <stdarg.h>
#include <microhttpd.h>

void ping903(void);


/* Configuration file parser */

/* Callback mode */
enum {
	CF_PARSE,      /* parse input statement */
	CF_SERIALIZE   /* serialize configuration setting */
};

typedef enum {
	CF_HEREDOC_START,
	CF_HEREDOC_NEXT,
	CF_HEREDOC_STOP
} CF_HEREDOC_OPCODE;

union cf_callback_arg {
	struct json_value *output;  /* Output value */
	struct cf_line {
		char const *val;    /* Statement value (after keyword) */
		char const *file;   /* Statement location: file name */
		unsigned line;      /*     ...  and input line number */
	} input;
	struct cf_heredoc {
		CF_HEREDOC_OPCODE op;
		char const *val;    /* Statement value (after keyword) */
		char const *file;   /* Statement location: file name */
		unsigned line;      /*     ...  and input line number */
	} heredoc;		
};

enum {
	CF_RET_OK,
	CF_RET_FAIL,
	CF_RET_IGNORE
};

/* Configuration parser callback function */
typedef int (*CF_CALLBACK)(int mode, union cf_callback_arg *arg, void *data);

/* Built-in statement types */
enum {
	STMT_T_STRING,
	STMT_T_ULONG,
	STMT_T_BOOL,
	STMT_T_CALLBACK,
	STMT_T_HEREDOC
};

int readconfig(char const *file);
int cf_trusted_ip(int mode, union cf_callback_arg *arg, void *data);
int cf_trusted_ip_heredoc(int mode, union cf_callback_arg *arg, void *data);
int cf_syslog_facility(int mode, union cf_callback_arg *arg, void *data);
int cf_auth(int mode, union cf_callback_arg *arg, void *data);

char *get_remote_ip(struct MHD_Connection *conn);

enum {
	HOST_STAT_INIT,      /* Initial state: data are being collected */
	HOST_STAT_VALID,     /* The record is valid */
	HOST_STAT_PENDING,   /* The record is valid; new data are being
				collected */
	HOST_STAT_INVALID    /* The record is invalid (host unreachable) */
};

struct host_stat {
	int status;              /* Host status: one of the above constants. */
	struct timeval start_tv; /* Time when the first echo was sent. */
	struct timeval stop_tv;  /* Time when the last reply was received. */
	unsigned long xmit_count;/* Number of echoes transmitted. */
	unsigned long recv_count;/* Number of replies received. */
	unsigned long dup_count; /* Number of duplicated replies. */
	double tmin;             /* minimum round trip time */
	double tmax;             /* maximum round trip time */
	double avg;              /* average round trip time */
	double stddev;           /* standard deviation */
};

static inline int host_stat_is_valid(struct host_stat const *hs) {
	return hs->status == HOST_STAT_VALID || hs->status == HOST_STAT_PENDING;
}

typedef struct hostping {
	char *name;               /* Symbolic host name or IP address. */
	struct sockaddr *addr;    /* Host address. */
	socklen_t addrlen;        /* Address length. */
	int immutable;            /* Is this host entry immutable */
	
	pthread_mutex_t mutex;
	
	/* Current ping statistics */
	struct timeval start_tv;  /* Time when the first echo was sent. */
	struct timeval xmit_tv;   /* Time when the latest echo was sent. */
	struct timeval recv_tv;   /* Time when the last reply was received. */
	unsigned long xmit_count; /* Number of echoes transmitted this far. */
	unsigned long recv_count; /* Number of replies received. */
	unsigned long dup_count;  /* Number of duplicate replies. */
	double tmin;              /* minimum round trip time */
	double tmax;              /* maximum round trip time */
	double tsum;              /* sum of all times, for doing average */
	double tsumsq;            /* sum of all times squared, for stddev. */

	/* Last probe statistics */
	struct host_stat stat_last;

	/* Pointers to the previous and next host in the list. */
	struct hostping *prev, *next;

	/* Reply counters for each echo request.  Actual dimension is
	 * ping_count entries.
	 */
	int nreply[1];
} HOSTPING;

/* Red-black tree definitions and prototypes. */
typedef struct rbt_tree RBT_TREE;
RBT_TREE *rbt_tree_create(int (*cmp)(HOSTPING*, HOSTPING*));

/* Return codes for rbt_lookup_or_insert_node and derived functions. */
   
typedef enum {
	RBT_LOOKUP_SUCCESS,  /* Entry was found. */
	RBT_LOOKUP_NOENT,    /* Entry was not found (perhaps created). */
	RBT_LOOKUP_FAILURE   /* Memory allocation error. */
} RBT_LOOKUP_RESULT;

HOSTPING *rbt_lookup(RBT_TREE *tree, HOSTPING *key);
RBT_LOOKUP_RESULT rbt_insert(RBT_TREE *tree, HOSTPING *host);
void rbt_delete(RBT_TREE *tree, HOSTPING *host);

void pinger_setup(void);
RBT_LOOKUP_RESULT pinger_host_add(char const *name, struct sockaddr *addr,
				  socklen_t addrlen);

/* Return codes for pinger update functions */
enum {
	UPD_OK,          /* Update successful. */
	UPD_NOMEM,       /* Not enough memory. */
	UPD_EXISTS,      /* When adding: such entry already exists. */
	                 /* When deleting: such entry does not exist. */
	UPD_NORESOLV     /* Host name does not resolve */
};

int pinger_host_delete_by_name(char const *name);
int pinger_host_add_name(char const *name);
int pinger_hostlist_set(struct json_value *obj, char const **err_text,
			int *err_pos);


extern int verbose;
extern int fatal_signals[];
extern char *httpd_addr;
extern int httpd_access_log;
extern int httpd_log_verbose;
extern char *pidfile;
extern unsigned long probe_interval;
extern unsigned long ping_interval;
extern unsigned long ping_count;
extern unsigned long ping_tolerance;
extern size_t data_length;
extern unsigned int httpd_backlog_size;

struct json_value *config_to_json(void);

/* Return codes for get_ function family. */
enum {
	GET_OK,     /* Success. */
	GET_NOMEM,  /* Not enough memory. */
	GET_NOENT   /* Requested entry not found. */
	            /* Obviously, this is never returned by get_all_*
		       functions. */
};
int get_hostname_stat(char const *name, struct json_value **retval);
int get_all_hosts(struct json_value **);
int get_all_host_stat(struct json_value **);
int get_host_matches(struct json_value *);

int file_read_ip_list(FILE *fp, char const *fname);

void p903_init(void);
void p903_update_commit(void);
void *p903_sender(void *p);
void *p903_receiver(void *p);
void *p903_scheduler(void *p);
