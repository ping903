/* This file is part of Ping903
   Copyright (C) 2020-2025 Sergey Poznyakoff
  
   Ping903 is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
  
   Ping903 is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
  
   You should have received a copy of the GNU General Public License
   along with Ping903.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <tcpd.h>
#include <microhttpd.h>
#include "defs.h"

int
p903_httpd_acl(void *cls, const struct sockaddr *addr, socklen_t addrlen)
{
	struct request_info req;
	request_init(&req,
		     RQ_DAEMON, progname,
		     RQ_CLIENT_SIN, addr,
		     RQ_SERVER_SIN, cls,
		     NULL);
	sock_methods(&req);
	return hosts_access(&req) ? MHD_YES : MHD_NO;
}
