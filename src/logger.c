/* This file is part of Ping903.
   Copyright (C) 2020-2025 Sergey Poznyakoff

   Ping903 is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Ping903 is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Ping903.  If not, see <http://www.gnu.org/licenses/>. */
#include <config.h>
#include <stdio.h>
#include <string.h>
#include <syslog.h>
#include "ping903.h"
#include "json.h"

char *progname;

static int facility = LOG_DAEMON;

static void
vlog_syslog(int prio, char const *fmt, va_list ap)
{
	vsyslog(prio, fmt, ap);
}

static void
vlog_stderr(int prio, char const *fmt, va_list ap)
{
	fprintf(stderr, "%s: ", progname);
	vfprintf(stderr, fmt, ap);
	fputc('\n', stderr);
}

static void (*vlog)(int prio, char const *fmt, va_list ap) = vlog_stderr;

void
vlogger(int prio, char const *fmt, va_list ap)
{
	vlog(prio, fmt, ap);
}

void
fatal(char const *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	vlog(LOG_CRIT, fmt, ap);
	va_end(ap);
}

void
error(char const *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	vlog(LOG_ERR, fmt, ap);
	va_end(ap);
}

void
info(char const *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	vlog(LOG_INFO, fmt, ap);
	va_end(ap);
}

void
syslog_enable(void)
{
	openlog(progname, LOG_PID, facility);
	vlog = vlog_syslog;
}

static struct facility {
	char const *s;
	int n;
} facilities[] = {
	{ "auth", LOG_AUTH },
	{ "authpriv", LOG_AUTHPRIV },
	{ "cron", LOG_CRON },
	{ "daemon", LOG_DAEMON },
	{ "ftp", LOG_FTP },
	{ "kern", LOG_KERN },
	{ "lpr", LOG_LPR },
	{ "mail", LOG_MAIL },
	{ "news", LOG_NEWS },
#ifdef LOG_AUTH
	{ "security", LOG_AUTH },
#endif
	{ "syslog", LOG_SYSLOG },
	{ "user", LOG_USER },
	{ "uucp", LOG_UUCP },
	{ "local0", LOG_LOCAL0 },
	{ "local1", LOG_LOCAL1 },
	{ "local2", LOG_LOCAL2 },
	{ "local3", LOG_LOCAL3 },
	{ "local4", LOG_LOCAL4 },
	{ "local5", LOG_LOCAL5 },
	{ "local6", LOG_LOCAL6 },
	{ "local7", LOG_LOCAL7 },
	{ NULL, -1 }
};
	  
int
set_log_facility(char const *arg)
{
	struct facility *f;
	for (f = facilities; f->s; f++)
		if (strcmp(f->s, arg) == 0)
			break;
	if (f->n == -1)
		return -1;
	facility = f->n;
	return 0;
}

int
cf_syslog_facility(int mode, union cf_callback_arg *arg, void *data)
{
	if (mode == CF_PARSE) {
		if (set_log_facility(arg->input.val)) {
			error("%s:%d: unknown syslog facility",
			      arg->input.file, arg->input.line);
			return CF_RET_FAIL;
		}
	} else {
		struct facility *f;
		for (f = facilities; f->s; f++) {
			if (f->n == facility) {
				struct json_value *val = json_new_string(f->s);
				if (!val) {
					fatal("out of memory");
					return CF_RET_FAIL;
				}
				arg->output = val;
				return CF_RET_OK;
			}
		}
		fatal("can't decode the current facility; please report");
		return CF_RET_FAIL;
	}
	return CF_RET_OK;
}

void
set_progname(char const *arg)
{
	char *p = strrchr(arg, '/');
	if (p)
		p++;
	else
		p = (char*) arg;
	progname = p;
}

