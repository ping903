/* This file is part of Ping903
   Copyright (C) 2020-2025 Sergey Poznyakoff
  
   Ping903 is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
  
   Ping903 is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
  
   You should have received a copy of the GNU General Public License
   along with Ping903.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <config.h>
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <syslog.h>
#include <ctype.h>
#include <errno.h>
#include <string.h>
#include <microhttpd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <fnmatch.h>
#include "ping903.h"
#include "json.h"
#include "defs.h"
#include "basicauth.h"

char *httpd_addr;
int httpd_access_log = 0;
int httpd_log_verbose = 0;
unsigned int httpd_backlog_size = SOMAXCONN;


struct strentry {
	struct strentry *next;
	char str[1];
};

struct strlist {
	struct strentry *head, *tail;
};

static int
strlist_add(struct strlist *slist, const char *str, size_t len)
{
	struct strentry *sent = malloc(sizeof(*sent) + len);
	if (!sent)
		return -1;

	memcpy(sent->str, str, len);
	sent->str[len] = 0;
	sent->next = NULL;
	
	if (slist->tail)
		slist->tail->next = sent;
	else
		slist->head = sent;
	slist->tail = sent;
	return 0;
}

static void
strlist_free(struct strlist *slist)
{
	struct strentry *sent = slist->head;
	while (sent) {
		struct strentry *next = sent->next;
		free(sent);
		sent = next;
	}
	slist->head = slist->tail = NULL;
}

static int
strlist_from_csv(struct strlist *slist, const char *s)
{
	while (*s) {
		size_t n = strcspn(s, ",");
		if (n > 0) {
			if (strlist_add(slist, s, n)) {
				strlist_free(slist);
				return -1;
			}
		}
		s += n;
		if (*s)
			s++;
	}
	return 0;
}

static int
open_node(char const *node, char const *serv, struct sockaddr **saddr)
{
	struct addrinfo hints;
	struct addrinfo *res;
	int reuse;
	int fd;
	int rc;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	rc = getaddrinfo(node, serv, &hints, &res);
	if (rc) {
		error("%s: %s", node, gai_strerror(rc));
		exit(1);
	}

	fd = socket(res->ai_family, res->ai_socktype, 0);
	if (fd == -1) {
		error("socket: %s", strerror(errno));
		exit(1);
	}
	reuse = 1;
	setsockopt(fd, SOL_SOCKET, SO_REUSEADDR,
		   &reuse, sizeof(reuse));
	if (bind(fd, res->ai_addr, res->ai_addrlen) == -1) {
		error("bind: %s", strerror(errno));
		exit(1);
	}

	if (listen(fd, 8) == -1) {
		error("listen: %s", strerror(errno));
		exit(1);
	}

	if (saddr) {
		*saddr = emalloc(res->ai_addrlen);
		memcpy(*saddr, res->ai_addr, res->ai_addrlen);
	}
	freeaddrinfo(res);

	return fd;
}

static int
open_listener(char const *addr, struct sockaddr **saddr)
{
	size_t len;
	int fd;

	if (!addr)
		return open_node(DEFAULT_ADDRESS, DEFAULT_SERVICE, saddr);
	
	len = strcspn(addr, ":");
	if (len == 0)
		fd = open_node(DEFAULT_ADDRESS, addr + 1, saddr);
	else if (addr[len] == 0)
		fd = open_node(addr, DEFAULT_SERVICE, saddr);
	else {
		char *node;
		node = emalloc(len + 1);
		memcpy(node, addr, len);
		node[len] = 0;
		fd = open_node(node, addr + len + 1, saddr);
		free(node);
	}
	return fd;
}

static void
p903_httpd_logger(void *arg, const char *fmt, va_list ap)
{
	vlogger(LOG_ERR, fmt, ap);
}

static void
p903_httpd_panic(void *cls, const char *file, unsigned int line,
		 const char *reason)
{
	if (reason)
		fatal("%s:%d: MHD PANIC: %s", file, line, reason);
	else
		fatal("%s:%d: MHD PANIC", file, line);
	abort();
}

#if HAVE_LIBWRAP
extern int p903_httpd_acl(void *cls, const struct sockaddr *addr,
			  socklen_t addrlen);
#else
# define p903_httpd_acl NULL
#endif


static int
reassemble_get_args(void *cls, enum MHD_ValueKind kind,
		    const char *key, const char *value)
{
	struct strlist *slist = cls;
	strlist_add(slist, "&", 1);
	strlist_add(slist, key, strlen(key));
	if (value) {
		strlist_add(slist, "=", 1);
		strlist_add(slist, value, strlen(value));
	}
	return MHD_YES;
}

static void
http_log(struct MHD_Connection *connection,
	 char const *method, char const *url,
	 int status, char const *str)
{
	char *ipstr;
	char const *host, *referer, *user_agent;
	time_t t;
	struct tm *tm;
	char tbuf[30];
	struct strlist slist = { NULL, NULL };
	char *reqs = NULL;
	
	if (!httpd_access_log)
		return;
	if (!httpd_log_verbose)
		str = NULL;
	ipstr = get_remote_ip(connection);

	host = MHD_lookup_connection_value(connection,
					   MHD_HEADER_KIND,
					   MHD_HTTP_HEADER_HOST);
	referer = MHD_lookup_connection_value(connection,
					      MHD_HEADER_KIND,
					      MHD_HTTP_HEADER_REFERER);
	user_agent = MHD_lookup_connection_value(connection,
						 MHD_HEADER_KIND,
						 MHD_HTTP_HEADER_USER_AGENT);
	t = time(NULL);
	tm = localtime(&t);
	strftime(tbuf, sizeof(tbuf), "[%d/%b/%Y:%H:%M:%S %z]", tm);

	MHD_get_connection_values(connection, MHD_GET_ARGUMENT_KIND,
				  &reassemble_get_args, &slist);
	if (slist.head) {
		size_t len = 0;
		struct strentry *sent;
		char *p;
		
		for (sent = slist.head; sent; sent = sent->next)
			len += strlen(sent->str);
		p = malloc(len + 1);
		if (p) {
			reqs = p;
			*p++ = '?';
			for (sent = slist.head->next; sent; sent = sent->next)
				p += strlen(strcpy(p, sent->str));
			*p = 0;
		}
		strlist_free(&slist);
	}
	
	info("%s %s - - %s \"%s %s%s\" \"%.1024s\" %3d \"%s\" \"%s\"",
	     host, ipstr, tbuf, method, url, reqs ? reqs : "",
	     str ? str : "", status,
	     referer ? referer : "",
	     user_agent ? user_agent : "");
	free(reqs);
	free(ipstr);
}

static int
http_response(struct MHD_Connection *conn,
	      char const *method, char const *url, int status)
{
	int ret;
	struct MHD_Response *resp =
		MHD_create_response_from_buffer(0,
						NULL,
						MHD_RESPMEM_PERSISTENT);
	http_log(conn, method, url, status, NULL);
	ret = MHD_queue_response(conn, status, resp);
	MHD_destroy_response(resp);
	return ret;
}

static int httpd_json_response(struct MHD_Connection *conn,
			       char const *url, char const *method,
			       int status,
			       struct json_value *val);

static int
http_response_detail(struct MHD_Connection *conn,
		     char const *method, char const *url, int status,
		     char const *message, int index)
{
	struct json_value *jv = NULL, *errobj = json_new_object();
		
	if (!errobj)
		goto err;
	if ((jv = json_new_string(message)) == NULL
	    || json_object_set(errobj, "message", jv))
		goto err;
	if (index) {
		if ((jv = json_new_number(index)) == NULL
		    || json_object_set(errobj, "index", jv))
			goto err;
	}
	return httpd_json_response(conn, url, method, status, errobj);
err:
	json_value_free(jv);
	json_value_free(errobj);
	return http_response(conn, method, url,
			     MHD_HTTP_INTERNAL_SERVER_ERROR);
}

struct strbuf {
	char *base;
	size_t off;
	size_t size;
	int err;
};

static void
strbuf_writer(void *closure, char const *text, size_t len)
{
	struct strbuf *sb = closure;
	if (sb->err)
		return;
	while (sb->off + len >= sb->size) {
		char *p = json_2nrealloc(sb->base, &sb->size, 1);
		if (!p) {
			error("not enough memory trying to format reply");
			sb->err = 1;
			return;
		}
		sb->base = p;
	}
	memcpy(sb->base + sb->off, text, len);
	sb->off += len;
}
	
static char *
json_to_str(struct json_value *obj)
{
	struct strbuf sb;
	struct json_format fmt = {
		.indent = 0,
		.precision = 5,
		.write = strbuf_writer,
		.data = &sb
	};

	memset(&sb, 0, sizeof(sb));
	json_value_format(obj, &fmt, 0);
	strbuf_writer(&sb, "", 1);

	if (sb.err) {
		free(sb.base);
		return NULL;
	}
	return sb.base;
}

static int
httpd_json_response(struct MHD_Connection *conn,
		    char const *url, char const *method,
		    int status,
		    struct json_value *val)
{
	char *reply;
	struct MHD_Response *resp;
	int ret;
	
	reply = json_to_str(val);
	json_value_free(val);
	if (!reply)
		return http_response(conn, method, url,
				  MHD_HTTP_INTERNAL_SERVER_ERROR);
	
	resp = MHD_create_response_from_buffer(strlen(reply),
					       reply, MHD_RESPMEM_MUST_COPY);
	http_log(conn, method, url, status, reply);
	free(reply);
	MHD_add_response_header(resp,
				MHD_HTTP_HEADER_CONTENT_TYPE,
				"application/json");
	ret = MHD_queue_response(conn, status, resp);
	MHD_destroy_response(resp);
	return ret;
}

static int try_auth(struct MHD_Connection *conn, const char *url,
		    const char *method, int *ret_val);

struct auth_flt_data
{
	struct MHD_Connection *conn;
	const char *url;
	const char *method;
	int err;
};
	
static int
auth_flt(char const *kw, struct json_value *val, void *data)
{
	int rc;
	struct auth_flt_data *adata = data;
	char *url = malloc(strlen(adata->url) + strlen(kw) + 2);
	if (!url) {
		adata->err = 1;
		return 0;
	}
	strcpy(url, adata->url);
	strcat(url, "/");
	strcat(url, kw);
	rc = try_auth(adata->conn, url, adata->method, NULL);
	free(url);
	return rc;
}

static int
attr_flt(char const *kw, struct json_value *val, void *data)
{
	return strcmp(kw, (char*)data);
}

static int
http_json_flt_response(struct MHD_Connection *conn,
		       const char *url, const char *method,
		       struct json_value *val, const char *suffix)
{
	int ret;
	
	if (!val)
		return http_response(conn, method, url,
				  MHD_HTTP_INTERNAL_SERVER_ERROR);
	while (*suffix == '/')
		suffix++;
	if (*suffix) {
		if (json_object_filter(val, attr_flt, (void*)suffix)) {
			ret = http_response(conn, method, url,
					    MHD_HTTP_INTERNAL_SERVER_ERROR);
			json_value_free(val);
			return ret;
		}
		if (!val->v.o->head) {
			ret = http_response(conn, method, url,
						MHD_HTTP_NOT_FOUND);
			json_value_free(val);
			return ret;
		}
	} else {
		struct auth_flt_data adata = {
			.conn = conn,
			.url = url,
			.method = method,
			.err = 0
		};
		if (json_object_filter(val, auth_flt, &adata) || adata.err) {
			ret = http_response(conn, method, url,
					    MHD_HTTP_INTERNAL_SERVER_ERROR);
			json_value_free(val);
			return ret;
		}
	}	
		
	return httpd_json_response(conn, url, method, MHD_HTTP_OK, val);
}

static struct json_value *
ident_to_json(void)
{
	struct json_value *obj, *jv;

	obj = json_new_object();
	if (!obj)
		return NULL;
	if (!(jv = json_new_string(PACKAGE))
	    || json_object_set(obj, "package", jv))
		goto err;
	if (!(jv = json_new_string(PACKAGE_VERSION))
	    || json_object_set(obj, "version", jv))
		goto err;
	if (!(jv = json_new_number(getpid()))
	    || json_object_set(obj, "pid", jv))
		goto err;
	return obj;
err:
	json_value_free(jv);
	json_value_free(obj);
	return NULL;
}

static int
ept_ident(struct MHD_Connection *conn,
	  const char *url, const char *method, const char *suffix,
	  struct json_value *unused)
{
	return http_json_flt_response(conn, url, method,
				      ident_to_json(), suffix);
}

static int
ept_config(struct MHD_Connection *conn,
	   const char *url, const char *method, const char *suffix,
	   struct json_value *unused)
{
	return http_json_flt_response(conn, url, method,
				      config_to_json(), suffix);
}

static int
ept_ip_delete(struct MHD_Connection *conn,
	      const char *url, const char *method, const char *suffix,
	      struct json_value *unused)
{
	int ret;

	while (*suffix == '/')
		suffix++;
	if (!*suffix)
		return http_response(conn, method, url, MHD_HTTP_FORBIDDEN);
	switch (pinger_host_delete_by_name(suffix)) {
	case UPD_OK:
		ret = http_response(conn, method, url, MHD_HTTP_OK);
		break;

	case UPD_EXISTS:
		ret = http_response_detail(conn, method, url,
					   MHD_HTTP_NOT_FOUND,
					   "No such host", 0);
		break;
		
	default:
		ret = http_response(conn, method, url,
				    MHD_HTTP_INTERNAL_SERVER_ERROR);
	}
	return ret;
}

static int
ept_ip_put(struct MHD_Connection *conn,
	   const char *url, const char *method, const char *suffix,
	   struct json_value *unused)
{
	while (*suffix == '/')
		suffix++;
	if (!*suffix)
		return http_response(conn, method, url, MHD_HTTP_FORBIDDEN);
	switch (pinger_host_add_name(suffix)) {
	case UPD_OK:
		break;
		
	case UPD_NORESOLV:
		return http_response_detail(conn, method, url,
					    MHD_HTTP_FORBIDDEN,
					    "Hostname does not resolve",
					    0);

	case UPD_NOMEM:
		return http_response(conn, method, url,
				     MHD_HTTP_INTERNAL_SERVER_ERROR);
		
	case UPD_EXISTS:
		return http_response_detail(conn, method, url,
					    MHD_HTTP_FORBIDDEN,
					    "Hostname already exists",
					    0);
	}

	return http_response(conn, method, url, MHD_HTTP_CREATED);
}

static int
ept_ip_post(struct MHD_Connection *conn,
	    const char *url, const char *method, const char *suffix,
	    struct json_value *obj)
{
	char const *err_text;
	int err_pos;
	int status;

	status = pinger_hostlist_set(obj, &err_text, &err_pos);
	if (!err_text && !err_pos)
		return http_response(conn, method, url, status);
	else
		return http_response_detail(conn, method, url, status,
					    err_text, err_pos);
}

struct host_stat_args_closure {
	struct strlist hostlist;
	struct strlist attrlist;
	int err;
};

static int
collect_host_stat_args(void *cls, enum MHD_ValueKind kind,
		       const char *key, const char *value)
{
	struct host_stat_args_closure *clos = cls;
	
	if (strcmp(key, "select") == 0 && value) {
		if (strlist_from_csv(&clos->hostlist, value)) {
			clos->err = 1;
			return MHD_NO;
		}
	} else if (strcmp(key, "attr") == 0 && value) {
		if (strlist_from_csv(&clos->attrlist, value)) {
			clos->err = 1;
			return MHD_NO;
		}
	} else {
		clos->err = 1;
		return MHD_NO;
	}
	return MHD_YES;
}

static void
host_stat_args_free(struct host_stat_args_closure *clos)
{
	strlist_free(&clos->hostlist);
	strlist_free(&clos->attrlist);
}

static int
get_selected_host_stat(struct strlist const *slist,
		       struct json_value **ret_arr)
{
	struct json_value *arr, *jv;
	struct strentry *sent;
	
	if (!(arr = json_new_array()))
		return GET_NOMEM;
	for (sent = slist->head; sent; sent = sent->next) {
		switch (get_hostname_stat(sent->str, &jv)) {
		case GET_OK:
			break;
			
		case GET_NOENT:
			jv = json_new_null();
			break;

		case GET_NOMEM:
			jv = NULL;
			break;
		}
		if (jv) {
			if (json_array_append(arr, jv) == 0)
				continue;
			json_value_free(jv);
		}
		/* Error: */
		json_value_free(arr);
		return -1;
	}
	*ret_arr = arr;
	return 0;
}

static int
attrlist_flt(char const *kw, struct json_value *val, void *data)
{
	struct strlist *slist = data;
	struct strentry *sent;

	for (sent = slist->head; sent; sent = sent->next) {
		if (strcmp(kw, sent->str) == 0)
			return 0;
	}
	return 1;
}

static int
attrlist_extract(struct json_value *val, struct strlist *slist)
{
	if (val && slist->head) {
		switch (val->type) {
		case json_array: {
			size_t i;

			for (i = 0; i < val->v.a->oc; i++) {
				if (attrlist_extract(val->v.a->ov[i], slist))
					return -1;
			}
			break;
		}
		case json_object:
			return json_object_filter(val, attrlist_flt, slist);
			break;
			
		default:
			/* Leave scalar values unchanged */
			break;
		}
	}
	return 0;
}

static int
ept_host_stat(struct MHD_Connection *conn,
	      const char *url, const char *method, const char *suffix,
	      struct json_value *unused)
{
	struct json_value *val;
	int rc, ret;
	struct host_stat_args_closure clos;
	
	while (*suffix == '/')
		suffix++;

	memset(&clos, 0, sizeof(clos));
	if (*suffix && strlist_add(&clos.hostlist, suffix, strlen(suffix)))
		return http_response(conn, method, url,
				     MHD_HTTP_INTERNAL_SERVER_ERROR);
	MHD_get_connection_values(conn, MHD_GET_ARGUMENT_KIND,
				  &collect_host_stat_args, &clos);
	if (clos.err) {
		host_stat_args_free(&clos);
		return http_response(conn, method, url, MHD_HTTP_BAD_REQUEST);
	}

	if (clos.hostlist.head) {
		rc = get_selected_host_stat(&clos.hostlist, &val);
	} else {
		rc = get_all_host_stat(&val);
	}

	if (attrlist_extract(val, &clos.attrlist)) {
		json_value_free(val);
		rc = GET_NOMEM;
	}		

	host_stat_args_free(&clos);
	
	switch (rc) {
	case GET_OK:
	case GET_NOENT:
		ret = httpd_json_response(conn, url, method, MHD_HTTP_OK, val);
		break;

	default:
		ret = http_response(conn, method, url,
				    MHD_HTTP_INTERNAL_SERVER_ERROR);
		
	}
	return ret;
}

static int
ept_ip_match(struct MHD_Connection *conn,
	     const char *url, const char *method, const char *suffix,
	     struct json_value *unused)
{
	struct json_value *ar;
	struct host_stat_args_closure clos;
	struct strentry *ent;

	while (*suffix == '/')
		suffix++;
	
	memset(&clos, 0, sizeof(clos));
	if (*suffix && strlist_add(&clos.hostlist, suffix, strlen(suffix)))
		return http_response(conn, method, url,
				     MHD_HTTP_INTERNAL_SERVER_ERROR);
	MHD_get_connection_values(conn, MHD_GET_ARGUMENT_KIND,
				  &collect_host_stat_args, &clos);
	if (clos.err || clos.attrlist.head) {
		host_stat_args_free(&clos);
		return http_response(conn, method, url, MHD_HTTP_BAD_REQUEST);
	}
	if (!clos.hostlist.head)
		return http_response(conn, method, url, MHD_HTTP_FORBIDDEN);

	if (!(ar = json_new_array()))
		goto err;
	for (ent = clos.hostlist.head; ent; ent = ent->next) {
		struct json_value *obj, *jv;
		
		if (!(obj = json_new_object()))
			goto err;
		if (json_array_append(ar, obj)) {
			json_value_free(obj);
			goto err;
		}
		if (!(jv = json_new_string(ent->str))
		    || json_object_set(obj, "name", jv)) {
			json_value_free(jv);
			goto err;
		}
		if (!(jv = json_new_array())
		    || json_object_set(obj, "hosts", jv)) {
			json_value_free(jv);
			goto err;
		}
	}	

	host_stat_args_free(&clos);

	if (get_host_matches(ar) == 0)
		return httpd_json_response(conn, url, method, MHD_HTTP_OK, ar);

err:
	host_stat_args_free(&clos);
	json_value_free(ar);
	return http_response(conn, method, url,
			     MHD_HTTP_INTERNAL_SERVER_ERROR);
}

enum auth_type {
	AUTH_NONE,
	AUTH_BASIC
};
	
struct auth_location {
	int type;
	char *url;
	size_t ulen;
	int wildcard;
	char *method;
	char *passwd_file;
	char *realm;
	struct auth_location *next;
};

static struct auth_location *auth_head, *auth_tail;

static int
cf_auth_parse(union cf_callback_arg *arg, void *data)
{
	int ac;
	char **av;
	char *endp;
	struct auth_location *loc;
	int type;
	char *passwd_file = NULL;
	char *realm = NULL;
		
	// auth TYPE METHOD URL [FILE [REALM]]
	enum { i_type, i_method, i_url, i_file, i_realm };

	switch (strsplit(arg->input.val, 5, &ac, &av, &endp)) {
	case STRSPLIT_OK:
		if (!*endp)
			break;
		/* fall through */
	case STRSPLIT_ERR:
		error("%s:%d: syntax error near %s",
		      arg->input.file, arg->input.line, endp);
		argcv_free(ac, av);
		return CF_RET_FAIL;
		
	case STRSPLIT_NOMEM:
		emalloc_die();
	}
	
        if (ac < 3) {
		error("%s:%d: syntax error",
		      arg->input.file, arg->input.line);
		argcv_free(ac, av);
		return CF_RET_FAIL;
	}

	if (strcmp(av[i_type], "basic") == 0) {
		type = AUTH_BASIC;
		if (ac < 5 && (!auth_tail || auth_tail->type != type)) {
			error("%s:%d: realm or password file name missing",
			      arg->input.file, arg->input.line);
			argcv_free(ac, av);
			return CF_RET_FAIL;
		}
		passwd_file = ac > i_file ? av[i_file] : auth_tail->passwd_file;
		realm = ac > i_realm ? av[i_realm] : auth_tail->realm;
	} else if (strcmp(av[i_type], "none") == 0) {
		type = AUTH_NONE;
		if (ac > 3) {
			error("%s:%d: too many arguments for this authorization type",
			      arg->input.file, arg->input.line);
			argcv_free(ac, av);
			return CF_RET_FAIL;
		}
			
	} else {
		error("%s:%d: unsupported authentication method",
		      arg->input.file, arg->input.line);
		argcv_free(ac, av);
		return CF_RET_FAIL;
	}

	loc = emalloc(sizeof(*loc));
	loc->type = type;
	loc->url = av[i_url];
	loc->ulen = strlen(loc->url);
	loc->wildcard = loc->url[strcspn(loc->url, "[]*?")] != 0;
	loc->method = av[i_method];
	loc->passwd_file = passwd_file;
	loc->realm = realm;
	loc->next = NULL;

	if (auth_tail)
		auth_tail->next = loc;
	else
		auth_head = loc;
	auth_tail = loc;

	/* All elements except 1st were transferred to loc, hence 1 as the
	   value of the first parameter. */
	argcv_free(1, av); 
	
	return CF_RET_OK;
}

static int
cf_auth_serialize(union cf_callback_arg *arg, void *data)
{
	struct json_value *ar, *obj = NULL, *jv = NULL;
	struct auth_location *auth;
	static char const *auth_type_str[] = {
		[AUTH_NONE] = "none",
		[AUTH_BASIC] = "basic"
	};

	if (!auth_head)
		return CF_RET_IGNORE;
	
	if (!(ar = json_new_array()))
		return CF_RET_FAIL;
	
	for (auth = auth_head; auth; auth = auth->next) {
		if (!(obj = json_new_object()))
			goto err;
		if (!(jv = json_new_string(auth_type_str[auth->type]))
		    || json_object_set(obj, "type", jv))
			goto err;
		if (!(jv = json_new_string(auth->url))
		    || json_object_set(obj, "url", jv))
			goto err;
		if (!(jv = json_new_string(auth->method))
		    || json_object_set(obj, "method", jv))
			goto err;
		if (auth->type == AUTH_BASIC) {
			if (auth->passwd_file
			    && (!(jv = json_new_string(auth->passwd_file))
				|| json_object_set(obj, "passwd-file", jv)))
				goto err;
			if (auth->realm
			    && (!(jv = json_new_string(auth->realm))
				|| json_object_set(obj, "realm", jv)))
				goto err;
		}
		if (json_array_append(ar, obj))
			goto err;
	}
	arg->output = ar;
	return CF_RET_OK;
err:
	json_value_free(jv);
	json_value_free(obj);
	json_value_free(ar);
	return CF_RET_FAIL;
}

int
cf_auth(int mode, union cf_callback_arg *arg, void *data)
{
	switch (mode) {
	case CF_PARSE:
		return cf_auth_parse(arg, data);
	case CF_SERIALIZE:
		return cf_auth_serialize(arg, data);
	}
	abort();
}

#define WWW_AUTH_PFX "Basic realm=\""
#define WWW_AUTH_SFX "\""

static char *
www_auth_hdr_format(char const *realm)
{
	size_t len = strlen(realm);
	char const *p;
	char *buf, *q;
	
	for (p = realm; *p; p++)
		if (*p == '\\' || *p == '"')
			len++;

	len += sizeof(WWW_AUTH_PFX) + sizeof(WWW_AUTH_SFX) - 1;

	buf = malloc(len);
	if (buf) {
		strcpy(buf, WWW_AUTH_PFX);
		q = buf + sizeof(WWW_AUTH_PFX) - 1;
		for (p = realm; *p; p++) {
			if (*p == '\\' || *p == '"')
				*q++ = '\\';
			*q++ = *p;
		}
		strcpy(q, WWW_AUTH_SFX);
	}
	return buf;
}

static int
http_unauthorized(struct MHD_Connection *conn,
		  char const *method, char const *url,
		  char const *str)
{
	int status = MHD_HTTP_UNAUTHORIZED;
	int ret;
	char *buf;
	struct MHD_Response *resp =
		MHD_create_response_from_buffer(0,
						NULL,
						MHD_RESPMEM_PERSISTENT);
	buf = www_auth_hdr_format(str);
	if (buf) {
		MHD_add_response_header(resp,
					MHD_HTTP_HEADER_WWW_AUTHENTICATE,
					buf);
		free(buf);
	} else {
		status = MHD_HTTP_INTERNAL_SERVER_ERROR;
	}
	http_log(conn, method, url, status, NULL);
	ret = MHD_queue_response(conn, status, resp);
	MHD_destroy_response(resp);
	return ret;
}

static int
try_auth(struct MHD_Connection *conn, const char *url, const char *method,
	 int *ret_val)
{
	struct auth_location *loc;
	size_t url_len;
	char *url_buf;
	char *p;
	char const *q;
	
	url_len = strlen(url);
	while (url_len > 1 && url[url_len-1] == '/')
		--url_len;
	url_buf = malloc(url_len + 1);
	if (!url_buf) {
		if (ret_val)
			*ret_val = http_response(conn, method, url,
						 MHD_HTTP_INTERNAL_SERVER_ERROR);
		return 1;
	}

	/* Copy url to url_buf squashing repeated '/' into a single slash */
	p = url_buf;
	q = url;
	while (*q) {
		if (!(*q == '/' && (q[1] == 0 || (q > url && q[-1] == '/'))))
			*p++ = *q;
		q++;
	}
	*p = 0;
	
	for (loc = auth_head; loc; loc = loc->next) {
		if (fnmatch(loc->method, method, 0))
			continue;
		if (loc->wildcard
		    ? fnmatch(loc->url, url_buf, 0) == 0
		    : (url_len >= loc->ulen
		       && strncmp(url_buf, loc->url, loc->ulen) == 0
		       && (url_buf[loc->ulen] == '/'
			   || url_buf[loc->ulen] == 0))) {
			char const *auth;

			free(url_buf);

			if (loc->type == AUTH_NONE)
				return 0;
			
			auth = MHD_lookup_connection_value(conn,
					       MHD_HEADER_KIND,
					       MHD_HTTP_HEADER_AUTHORIZATION);
			if (!auth) {
				if (ret_val)
					*ret_val = http_unauthorized(conn,
								     method,
								     url,
								     loc->realm);
				return 1;
			}
			switch (basicauth(loc->passwd_file, auth)) {
			case BASICAUTH_DENY:
			case BASICAUTH_BAD_INPUT:
				if (ret_val)
					*ret_val = http_unauthorized(conn,
								     method,
								     url,
								     loc->realm);
				return 1;				
			case BASICAUTH_ALLOW:
				return 0;
			case BASICAUTH_CANT_OPEN:
				error("can't open password file %s: %s",
				      loc->passwd_file, strerror(errno));
				/* fall through */
			default:
				if (ret_val)
					*ret_val = http_response(conn,
								 method,
								 url,
					      MHD_HTTP_INTERNAL_SERVER_ERROR);
				return 1;
			}
		}
	}
	free(url_buf);
	return 0;
}

typedef int (*ENDPOINT_HANDLER)(struct MHD_Connection *,
				const char *, const char *, const char *,
				struct json_value *);

enum {
	EPT_EXACT  = 0x01,
	EPT_PREFIX = 0x02
};

struct endpoint {
	char *url;
	int flags;
	char *method;
	ENDPOINT_HANDLER handler;
};

static struct endpoint endpoint[] = {
	{ "/id", EPT_PREFIX, MHD_HTTP_METHOD_GET, ept_ident },
	{ "/config/ip-list", EPT_PREFIX, MHD_HTTP_METHOD_PUT, ept_ip_put },
	{ "/config/ip-list", EPT_PREFIX, MHD_HTTP_METHOD_POST, ept_ip_post },
	{ "/config/ip-list", EPT_PREFIX, MHD_HTTP_METHOD_DELETE, ept_ip_delete },
	{ "/config", EPT_PREFIX, MHD_HTTP_METHOD_GET, ept_config },
	{ "/host",   EPT_PREFIX, MHD_HTTP_METHOD_GET, ept_host_stat },
	{ "/match",  EPT_PREFIX, MHD_HTTP_METHOD_GET, ept_ip_match },
	{ NULL }
};

static int
find_endpoint_handler(char const *url, char const *method,
		      ENDPOINT_HANDLER *ret_handler, char const **ret_suffix)
{
	struct endpoint *ept;
	int urlmatch = 0;
	for (ept = endpoint; ept->url; ept++) {
		if (ept->flags & EPT_EXACT) {
			if (strcmp(ept->url, url) == 0) {
				if (strcmp(ept->method, method)) {
					urlmatch++;
					continue;
				}
				*ret_handler = ept->handler;
				*ret_suffix = url + strlen(url);
				return MHD_HTTP_OK;
			}
		} else {
			size_t len = strlen(ept->url);
			if (strncmp(url, ept->url, len) == 0
			    && (url[len] == '/' || url[len] == 0)) {
				if (strcmp(ept->method, method)) {
					urlmatch++;
					continue;
				}
				*ret_handler = ept->handler;
				*ret_suffix = url + len;
				return MHD_HTTP_OK;
			}
		}
	}
	return urlmatch ? MHD_HTTP_METHOD_NOT_ALLOWED : MHD_HTTP_NOT_FOUND;
}

struct postbuf {
	char *base;
	size_t size;
	size_t len;
	ENDPOINT_HANDLER handler;
	char suffix[1];
};

static void
postbuf_free(struct postbuf *pb)
{
	free(pb->base);
	free(pb);
}

static int
postbuf_add(struct postbuf *pb, const char *str, size_t len)
{	
	while (pb->len + len >= pb->size) {
		char *np = json_2nrealloc(pb->base, &pb->size, 1);
		if (!np)
			return -1;
		pb->base = np;
	}
	memcpy(pb->base + pb->len, str, len);
	pb->len += len;
	return 0;
}

static int
p903_httpd_handler(void *cls,
		   struct MHD_Connection *conn,
		   const char *url, const char *method,
		   const char *version,
		   const char *upload_data, size_t *upload_data_size,
		   void **con_cls)
{
	ENDPOINT_HANDLER handler;
	char const *suffix;
	int status;

	if (*con_cls == NULL && try_auth(conn, url, method, &status))
		return status;
	if (strcmp(method, MHD_HTTP_METHOD_POST) == 0) {
		struct postbuf *pb;
		
		if (*con_cls == NULL) {
			char const *ctype;
			
			status = find_endpoint_handler(url, method, &handler,
						       &suffix);
			if (status != MHD_HTTP_OK)
				return http_response(conn, method, url, status);

			ctype = MHD_lookup_connection_value(conn,
							    MHD_HEADER_KIND,
							    MHD_HTTP_HEADER_CONTENT_TYPE);
			if (!ctype || strcmp(ctype, "application/json"))
				return http_response(conn, method, url,
						  MHD_HTTP_NOT_ACCEPTABLE);

			pb = malloc(sizeof(*pb) + strlen(suffix));
			if (!pb)
				return http_response(conn, method, url,
						  MHD_HTTP_INTERNAL_SERVER_ERROR);
			pb->base = NULL;
			pb->size = 0;
			pb->len  = 0;
			pb->handler = handler;
			strcpy(pb->suffix, suffix);
			*con_cls = pb;
		} else {
			pb = *con_cls;
			if (*upload_data_size != 0) {
				if (postbuf_add(pb, upload_data,
						*upload_data_size)) {
					postbuf_free(pb);
					return http_response(conn, method, url,
					        MHD_HTTP_INTERNAL_SERVER_ERROR);
				}
				*upload_data_size = 0;
			} else {
				char *endp;
				int rc;
				struct json_value *jv;
				
				if (postbuf_add(pb, "", 1)) {
					postbuf_free(pb);
					return http_response(conn, method, url,
					        MHD_HTTP_INTERNAL_SERVER_ERROR);
				}
				rc = json_parse_string(pb->base, &jv, &endp);
				if (rc != JSON_E_NOERR) {
					error("%s: %s near #%ld", url,
					      json_strerror(rc),
					      endp - pb->base);
					return http_response(conn, method, url,
							  MHD_HTTP_BAD_REQUEST);
				}
				rc = pb->handler(conn, url, method, pb->suffix,
						 jv);
				postbuf_free(pb);
				json_value_free(jv);
				return rc;
			}
		}
		return MHD_YES;
	} else if (*con_cls == NULL) {
		/*
		 * According to https://bugs.gnunet.org/view.php?id=7242#c19782,
		 * MHD_YES must be returned if con_cls points to NULL no matter
		 * what HTTP method is bieng used.
		 */
		static int first_time_marker;
		*con_cls = &first_time_marker;
		return MHD_YES;
	}

	/* For all other methods */
	status = find_endpoint_handler(url, method, &handler, &suffix);
	if (status != MHD_HTTP_OK)
		return http_response(conn, method, url, status); 
	return handler(conn, url, method, suffix, NULL);
}

static void
sigign(int sig)
{
}

void
ping903(void)
{
	struct MHD_Daemon *mhd;
	sigset_t sigs;
	int i;
	pthread_t tid;
        int fd = -1;
	struct sockaddr *server_addr;
	struct sigaction act;

	p903_init();
	
	fd = open_listener(httpd_addr, &server_addr);
	
	act.sa_flags = 0;
	sigemptyset(&act.sa_mask);
	act.sa_handler = sigign;

	/* Block the 'fatal signals' and SIGPIPE in the handling thread */
	sigemptyset(&sigs);
	for (i = 0; fatal_signals[i]; i++) {
		sigaddset(&sigs, fatal_signals[i]);
		sigaction(fatal_signals[i], &act, NULL);
	}
	
	sigaddset(&sigs, SIGPIPE);

	pthread_sigmask(SIG_BLOCK, &sigs, NULL);
	MHD_set_panic_func(p903_httpd_panic, NULL);
	mhd = MHD_start_daemon(MHD_USE_INTERNAL_POLLING_THREAD
			       | MHD_USE_ERROR_LOG, 0,
			       p903_httpd_acl, server_addr,
			       p903_httpd_handler, NULL,
			       MHD_OPTION_LISTEN_SOCKET, fd,
			       MHD_OPTION_EXTERNAL_LOGGER, p903_httpd_logger, NULL,
			       MHD_OPTION_LISTEN_BACKLOG_SIZE, httpd_backlog_size,
			       MHD_OPTION_END);
	
	/* Unblock only the fatal signals */
	sigdelset(&sigs, SIGPIPE);
	pthread_sigmask(SIG_UNBLOCK, &sigs, NULL);

	/* Start the Receiver thread */
	pthread_create(&tid, NULL, p903_receiver, NULL);
	/* Start the Sender thread */
	pthread_create(&tid, NULL, p903_sender, NULL);
	/* Start the Scheduler thread */
	pthread_create(&tid, NULL, p903_scheduler, NULL);

	/* Wait for signal to arrive */
	sigwait(&sigs, &i);
	info("shutting down on signal \"%s\"", strsignal(i));
	MHD_stop_daemon(mhd);
	p903_update_commit();
}
