/* This file is part of Ping903
   Copyright (C) 2020-2025 Sergey Poznyakoff
  
   Ping903 is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
  
   Ping903 is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
  
   You should have received a copy of the GNU General Public License
   along with Ping903.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdarg.h>

#ifndef DEFAULT_ADDRESS
# define DEFAULT_ADDRESS "127.0.0.1"
#endif
#ifndef DEFAULT_SERVICE
# define DEFAULT_SERVICE "8080"
#endif
#ifndef SYSCONFDIR
# define SYSCONFDIR "/etc"
#endif
#ifndef LOCALSTATEDIR
# define LOCALSTATEDIR "/var/lib"
#endif
#ifndef PACKAGESTATEDIR
# define PACKAGESTATEDIR LOCALSTATEDIR "/" PACKAGE
#endif
#define DEFAULT_CONFIG_FILE SYSCONFDIR "/" PACKAGE ".conf"
#define MUTABLE_IP_LIST_FILE PACKAGESTATEDIR "/ip-list"
#define CRED_FILE_NAME ".ping903.cred"

#define COPYLEFT "\
Copyright (C) 2020-2025 Sergey Poznyakoff\n\
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>\n\
This is free software: you are free to change and redistribute it.\n\
There is NO WARRANTY, to the extent permitted by law.\n\
\n"

void emalloc_die(void);
void *emalloc(size_t s);
void *ecalloc(size_t n, size_t s);
char *estrdup(char const *s);
void *e2nrealloc(void *p, size_t *pn, size_t s);

extern char *progname;

#define ATTR_PRINTFLIKE(fmt,narg)				\
	__attribute__ ((__format__ (__printf__, fmt, narg)))

void vlogger(int prio, char const *fmt, va_list ap);
void fatal(char const *fmt, ...) ATTR_PRINTFLIKE(1,2);
void error(char const *fmt, ...) ATTR_PRINTFLIKE(1,2);
void info(char const *fmt, ...) ATTR_PRINTFLIKE(1,2);
void syslog_enable(void);
void set_progname(char const *arg);
int set_log_facility(char const *arg);

enum {
	STRSPLIT_OK,
	STRSPLIT_NOMEM,
	STRSPLIT_ERR
};

int strsplit(char const *str, int max, int *ret_ac, char ***ret_av,
	     char **endp);
void argcv_free(int ac, char **av);
