#!/bin/sh
#! -*-perl-*-
eval 'exec perl -x -S $0 ${1+"$@"}'
    if 0;
# This file is part of Ping903
# Copyright (C) 2020-2025 Sergey Poznyakoff
# 
# Ping903 is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# Ping903 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Ping903.  If not, see <http://www.gnu.org/licenses/>.

=head1 NAME

ipadd - adds a single IP to the ping903 ip list

=head1 SYNOPSIS

B<ipadd>
[B<-U> I<URL>]
[B<--url>=I<URL>]
I<IP>

=head1 DESCRIPTION

Adds I<IP> to the mutable IP list of the running B<ping903> daemon.

=head1 OPTIONS

=head2 General options

=over 4

=item B<-U>, B<--url>=I<URL>

URL of the running L<ping903> daemon.  Default is C<http://localhost:8080>.

=back

=head2 Informative options

=over 4

=item B<-?>

Display short help summary.

=item B<--usage>

Display command line usage summary.

=item B<--help>

Display a detailed program manual.

=back

=head1 SEE ALSO

L<ping903>,
L<DBI>.

=cut

use strict;
use warnings;
use LWP::UserAgent;
use JSON;
use Getopt::Long qw(:config gnu_getopt no_ignore_case);
use Pod::Usage;
use Pod::Man;

my $baseurl = 'http://localhost:8080';

GetOptions(
    'U|url=s' => \$baseurl,
    'help' => sub {
	pod2usage(-exitstatus => 0, -verbose => 2);
    },
    'usage' => sub {
	pod2usage(-exitstatus => 0, -verbose => 0);
    },
    'hh|?' => sub {
	pod2usage(-message => "dbload - load IP addresses to ping903",
		  -exitstatus => 0);
    },
) or exit(1);

my $ip = shift @ARGV or die "not enough arguments";
die "too many arguments; try `$0 --help' for more info\n" if @ARGV;

my $ua = new LWP::UserAgent;
my $response = $ua->put("$baseurl/config/ip-list/$ip");
unless ($response->is_success) {
    print $response->status_line,"\n";
    my $ctype = $response->header('Content-Type');
    if ($ctype && $ctype eq 'application/json') {
	my $expl = JSON->new->decode($response->decoded_content);
	if (exists($expl->{index})) {
	    print "Item $expl->{index}: ";
	}
	print "$expl->{message}\n";
    }
}


