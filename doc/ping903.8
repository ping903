.\" This file is part of ping903 -*- nroff -*-
.\" Copyright (C) 2020\(en2025 Sergey Poznyakoff
.\"
.\" Ping903 is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" Ping903 is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with Ping903.  If not, see <http://www.gnu.org/licenses/>.
.TH PING903 8 "January 11, 2025" "PING903" "System Administration"
.SH NAME
ping903 \- high-performance ICMP monitoring daemon
.SH SYNOPSIS
\fBping903\fR\
 [\fB\-fhsVv\fR]\
 [\fB\-c \fIFILE\fR]
.SH DESCRIPTION
\fBPing903\fR is a daemon designed to periodically monitor a very
large number of remote hosts using ICMP ECHO packets.  Hosts are
monitored in separate \fBprobes\fR, each probe consisting of a
predefined number of ICMP requests sent with certain interval between
them.  Probes are repeated periodically.
.PP
The round-trip statistics is collected for each host and is available
for querying via REST API.  A separate querying utility
.BR ping903q (1)
is provided, which allows the user to request the information about
each particular host, or all monitored hosts at once.
.PP
Upon startup the program reads its configuration from file
.BR /etc/ping903.conf ,
and detaches itself from the controlling terminal.  This daemon
becomes a \fIsupervisor process\fR for the monitor process, which it
spawns.  The supervisor controls that the monitoring process is
properly functioning and restarts it if it terminates abnormally.
.PP
The monitoring process, in turn, is responsible for periodic polling
of the configured hosts, maintaining the statistics information and
answering incoming REST requests.
.PP
The program logs its activities using syslog channel \fBdaemon\fR.
.PP
The set of monitored IP addresses consists of two parts.  The
.I immutable
IP list is supplied in the configuration file and cannot be altered at
run-time.  The
.I mutable
IP list, in contrast, is maintained via the REST API and thus can be
changed when the server runs.  The content of the mutable IP list is
preserved during server restarts.
.PP
Each 
.B probe\-interval
seconds, the daemon wakes up and probes each IP from the list.  Each
probe consists of
.B ping\-count
ICMP ECHO requests sent with intervals of
.B ping\-interval
seconds between each of them.  Obviously, these three parameters must
satisfy the following relation:
.PP
.EX
ping-count * ping-interval < probe-interval
.EE
.PP
At the end of each probe the following information is computed:
number of received echo replies, percentage of lost requests,
minimal, average and maximal round-trip time, and its standard
deviation.  If more than a predefined number of echo requests (the
failure \fItolerance\fR number) are lost, the status of the IP is
changed to "inactive".
.PP
An \fBHTTP\fR listener thread is responsible for returning the collected
statistics and serving other requests.  By default it listens for
queries on localhost, port 8080.  If compiled with the support for 
\fBlibwrap\fR, the access to the HTTP interface is controlled using
the files 
.B /etc/hosts.allow
and
.BR /etc/hosts.deny .
See
.BR hosts_access (5),
for details.
.PP
For information about accessing the statistics, see
.BR ping903q (1).
.PP
A utility for maintaining the mutable IP list is available from
<http://git.gnu.org.ua/cgit/ping903/mangemanche.git>.
.SH CONFIGURATION FILE
By default, the configuration is read from the file
.BR /etc/ping903.conf .
See
.BR ping903.conf (5),
for a detailed discussion of its syntax and available keywords.
It is OK if the file does not exist.  In this case
.B ping903
will use the following  built-in defaults:
.PP
.RS 4
.nf
data\-length 56
# ip\-list is empty
ping\-count 10
ping\-interval 1
probe\-interval 60
syslog\-facility daemon
tolerance 3

listen localhost:8080
access-log off
access-log-verbose off
http\-backlog\-size 128
# trusted\-ip is empty
.fi
.RE
.SH OPTIONS
.TP
\fB\-c \fIFILE\fR
Read configuration from \fIFILE\fR.  By default,
.B /etc/ping903.conf
is read.
.TP
.B \-f
Remain in foreground.
.TP
.B \-h
Print a short usage summary.
.TP
.B \-s
Don't start supervisor process.
.TP
.B \-V
Print program version, copyright information, and exit.
.TP
.B \-v
Turn on additional logging.  This option can be given several times to
request more verbose output.  If given single \fB\-v\fR option, the
program prints at the end of each probe the total number of echo
requests sent and replies received.  Two options (\fB\-vv\fR), enable
additional diagnostics of invalid echo replies.  Three options enable
logging of each received echo reply, and four options enable verbose
logging of each echo request sent.  Notice that three or more
\fB\-v\fR options can produce a huge amount of output.
.SH BUGS
Only IPv4 is currently supported.
.SH SEE ALSO
.BR ping903.conf (5),
.BR ping903q (1).
.SH COPYRIGHT
Copyright \(co 2020\(en2025 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_.\\-]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:
