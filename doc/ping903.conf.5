.\" This file is part of ping903 -*- nroff -*-
.\" Copyright (C) 2020\(en2025 Sergey Poznyakoff
.\"
.\" Ping903 is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" Ping903 is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with Ping903.  If not, see <http://www.gnu.org/licenses/>.
.TH PING903.CONF 5 "January 11, 2025" "PING903.CONF" "File Formats Manual"
.SH NAME
ping903.conf \- configuration file for high-performance ICMP monitoring daemon
.SH DESCRIPTION
The file
.B /etc/ping903.conf
is read by
.BR ping903 (8)
at startup.  It consists of a series of \fIstatements\fR, each
statement occupying a separate line.  A statement consists of a
keyword and argument, separated by one or more whitespace characters.
Leading and trailing whitespace characters are ignored.
.PP
The pound sign (\fB#\fR) appearing as the first non-whitespace
character on a line introduces a comment.  Such lines are ignored.
Empty lines are ignored as well.
.PP
Maximum length of a statement line is limited to 1022 characters.
Lines longer than that cause syntax error to be reported and are
ignored otherwise.
.SH STATEMENTS
.SS Server configuration
.TP
\fBpidfile \fIFILE\fR
Instructs \fBping903\fR to write its PID to the specified file.  If
running with supervisor process (the default), the supervisor's PID
will be written.

By default no pidfile is created.
.TP
\fBsyslog\-facility \fINAME\fR
Configures the syslog facility to use.  Valid arguments are
.BR auth ,
.BR authpriv ,
.BR cron ,
.BR daemon " (default),"
.BR ftp ,
.BR kern ,
.BR lpr ,
.BR mail ,
.BR news ,
.BR security ,
.BR syslog ,
.BR user ,
.BR uucp ,
and
.B local0
through
.BR local7.
.SS Monitoring setup
.TP
\fBip\-list \fIFILE\fR
Name of the file supplying a list of IP addresses (or hostnames) to
monitor.  Each IP must be listed on a separate line.  Empty lines,
leading and trailing whitespace are ignored.  Comments are introduced
by a hash sign as the first non-whitespace character on the line.
.TP
\fBip\-list <<\fIWORD\fR
A \fIhere-document\fR version of the above statement.  \fIWORD\fR is
an arbitrary word.  The material following this statement is scanned
up to the line consisting only of \fIWORD\fR (surrounding whitespace
allowed).  Each line in the collected material must contain a single IP
address or host name to be incorporated into IP list.

Empty lines and comments are allowed within \fIhere-document\fR.
.PP
Multiple \fBip\-list\fR statements of any form accumulate.
.TP
\fBprobe\-interval \fIN\fR
Sets interval (in seconds) between subsequent probes.  Default is 60
(one minute).
.TP
\fBping\-count \fIN\fR
Configures the number of ICMP echo requests to be sent to each host
within a single probe.  Default is 10.
.TP
\fBping\-interval \fIN\fR
Configures the time between two subsequent echo requests in a probe.
.TP
\fBtolerance \fIN\fR
Failure tolerance.  This is the maximum number of echo requests in a
probe that may remain unanswered without declaring the host as dead.
Default is 3, i.e. a host is declared dead if more than 3 pings out of
each 10 are lost.
.TP
\fBdata\-length \fIN\fR
Number of additional bytes to send in each ICMP echo requests (apart from
the timestamp).  Default is 56.
.SS HTTP responder configuration
.TP
\fBlisten \fIIPADDR\fB:\fIPORT\fR
Listen for incoming HTTP requests on the given IP address and port.
\fIIPADDR\fR is a IPv4 address (or hostname) to listen on.  \fIPORT\fR
is the port number of 
symbolic service name from
.BR /etc/services .
Either \fIIPADDR\fR or \fIPORT\fR (but not both) can be omitted.  Missing
\fIIPADDR\fR is equivalent to 127.0.0.1 (note, that in this case the
colon before \fIPORT\fR must be present).  Missing \fIPORT\fR means
the default port number (8080).  

To summarize, possible arguments are:
\fIIPADDR\fB:\fIPORT\fR, \fIIPADDR\fR, or \fB:\fIPORT\fR.

Default is \fB0.0.0.0:8080\fR.
.TP
\fBhttp\-backlog\-size \fIN\fR
Configures the size of the
.BR listen (2)
backlog queue.  Default is the platform-dependent value
.BR SOMAXCONN ,
(128 on most GNU/Linux systems).
.TP
\fBaccess\-log \fIBOOL\fR
Enables the apache-style HTTP access logging.  Valid \fIBOOL\fR values are:
.BR 1 ,
.BR t ,
.BR true ,
.BR yes ,
or
.BR on ,
to enable logging, and
.BR 0 ,
.BR f ,
.BR nil ,
.BR false ,
.BR no ,
or
.BR off ,
to disable it.
.TP
\fBaccess\-log\-verbose \fIBOOL\fR
Configures whether to include dumps of JSON replies to access logs.
See above for the possible values of \fIBOOL\fR.
.TP
\fBtrusted\-ip \fICIDR\fR
Adds a CIDR to the list of trusted IP networks.  This list is used
when determining source IP address of a HTTP connection for the
purpose of logging.  By default, each log message contains the client
IP of the connection.  If the
.B X\-Forwarded\-For
header is present, its value overrides that IP. If the
header contains a comma-delimited list of IP addresses, this list is
processed from right to left, until either a not trusted IP or the
first IP in the list is encountered, whichever occurs first.

\fICIDR\fR is either an IPv4 address, or an address followed by slash
and the network mask in dotted quad, or the length of the network mask
in decimal.
.TP
\fBtrusted\-ip <<\fIWORD\fR
A \fIhere-document\fR version of the above statement.  See the
description of \fBip\-list\fR above for the description of
\fIhere-document\fR syntax.  The collected material must contain a
single CIDR per line.
.SS HTTP Authorization configuration
It is suggested that HTTP entry points be protected by the HTTP
authorization.  This is especially critical for the \fB/config\fR
URL and URLs below it, which allow the requester to modify \fBping903\fR
configuration.  This version of \fBping903\fR supports HTTP basic
authorization.
.TP
\fBauth basic\fR \fIMETHOD\fR \fIURL\fR \fIPWFILE\fR \fIREALM\fR
This statement enables basic authentication for \fIURL\fR when
accessed using the given HTTP \fIMETHOD\fR.

\fIMETHOD\fR is either the name of an HTTP method (\fBGET\fR, \fBPOST\fR,
etc.) or a wildcard \fB*\fR matching any method.

\fIURL\fR is the \fBping903\fR URL.  It is treated as a prefix, i.e.
the statement takes effect for anything below that URL as well.  The
\fIURL\fR can also contain
.BR glob (7)
wildcards.

\fIPWFILE\fR is the name of a textual file containing the list of
users and passwords for user authentication.  This file is compatible
with Apache \fBmod_authn_file\fR module and can be generated using
the Apache
.BR htpasswd (1)
utility.

\fIREALM\fR sets the realm to be used in the authentication.  The
realm is used by the client to determine what user name and password
to send for a given authenticated area.  If \fIREALM\fR contains
whitespace, it must be enclosed in double-quotes.  Within a
double-quoted string any occurrence of a double-quote or backslash
character must be escaped by prefixing it with a backslash.

These parameters are mandatory only for the very first occurrence of the
\fBauth\fR statement.  The statements that follow it may omit the
\fIIPFILE\fR and/or \fIREALM\fR, if they are the same as in the
preceding statement.  For example, the following statements protect
modifications to the \fBping903\fR configuration with basic
authorization:
.sp
.nf
auth basic POST /config /etc/ping903/htpasswd Modification
auth basic PUT  /config
.fi
.TP
\fBauth none\fR \fIMETHOD\fR \fIURL\fR
Disables authorization for this combination of \fIMETHOD\fR and
\fIURL\fR.  See \fBauth basic\fR for the description of \fIMETHOD\fR
and \fIURL\fR.

Use this statement to exempt an URL from authorization which is
otherwise required for its parent URL.  For example, the two
statements below require basic authorization for "/config", but not
for "/config/ip-list":
.sp
.nf
auth none GET /config/ip-list
auth basic GET /config /etc/ping903/htpasswd "Config Access"
.fi
.PP
Notice, that an incoming HTTP request is matched against each
\fBauth\fR statement in turn, in the order they appear in the
configuration file, and it is the first matching statement that
takes effect.  Therefore, be sure to place more specific METHOD/URL
combinations before more general ones.
.SH SEE ALSO
.BR ping903 (8),
.BR ping903q (1),
.BR ping903.cred (5).
.SH COPYRIGHT
Copyright \(co 2020\(en2025 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_.\\-]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:
