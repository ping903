.\" This file is part of ping903 -*- nroff -*-
.\" Copyright (C) 2020\(en2025 Sergey Poznyakoff
.\"
.\" Ping903 is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" Ping903 is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with Ping903.  If not, see <http://www.gnu.org/licenses/>.
.TH PING903Q 1 "January 11, 2025" "PING903Q" "User Commands"
.SH NAME
ping903q \- ping903 query tool
.SH SYNOPSIS
\fBping903q\fR\
 [\fB\-ahVv\fR]\
 [\fB\-f \fIFILE\fR]\
 [\fB\-R \fIREALM\fR]\
 [\fIHOST\fR...]
.PP
\fBping903q\fR\
  [\fB\-N\fR]\
  [\fB\-f \fIFILE\fR]\
  [\fB\-p \fIPREFIX\fR]\
  [\fB\-R \fIREALM\fR]\
  \fB\-H \fIHOST\fR\
  \fB\-c \fIRTA\fB,\fIPCT\fB%\fR\
  \fB\-w \fIRTA\fB,\fIPCT\fB%\fR
.PP
\fBping903q\fR\
  [\fB\-f \fIFILE\fR]\
  [\fB\-R \fIREALM\fR]\
  \fB\-m\fR
.SH DESCRIPTION
Queries the \fBping903\fR daemon for monitoring statistics about the
given \fBHOST\fRs.  To obtain information about all monitored hosts,
use the \fB\-a\fR option.
.PP
By default a one-line summary is displayed, which informs about the IP
and its current status ("alive" vs. "not alive").  Additional
information can be requested with the
.B \-v
command line option.  When given this option, the program will output
detailed information about round trip times and lost packets.
.PP
By default, the program attempts to connect to the default REST API
port of (localhost:8080).  If the file
.B /etc/ping903.conf
exists and contains the \fBlisten\fR statement, the value of this
statement will be used instead.  See
.BR ping903.conf (5),
for detailed description of the configuration file.
.SS Nagios check mode
When the \fB\-H\fR, \fB\-c\fR, and \fB\-w\fR options are used, the
program enters \fINagios check mode\fR.  In this mode its output
complies with the requirements for external \fBNagios\fR check
programs.
.SS Match mode
When invoked with the \fB\-m\fR option, \fBping903q\fR checks if
\fIHOST\fR is monitored by the server.  If so, it prints the matching
host names and exits with code 0.
.SS Basic authorization
If the server requires authorization, \fBping903q\fR will look into
the file \fB.ping903.cred\fR in the user home directory in order to
find authentication credentials (see
.BR ping903.cred (5),
for a detailed discussion of the file).  The file is scanned for
an entry that matches the server name and port (as given in the
\fBlisten\fR statement of the \fBping903.conf\fR file) and the
authorization \fIrealm\fR name presented by the server.  If such an
entry is found, the user name and password listed in it will be used
to send the authorized request.
.PP
The \fB\-R\fR option allows you to use a specific realm for
authorization.  In this case, the \fB.ping903.cred\fR file is scanned
at startup and the credentials found in it are used to authorize the
request, without sending unauthorized request first and consulting the
reply.
.SH EXIT CODE
When called with one argument, the program exits with code 0 (success)
if the IP is alive, 2 if it is not, and 3 if the host status is unknown.
.PP
When called without arguments, the program exits with code 0 if all
monitored IP addresses are alive, 2 if none of them is reachable and 1
if some of them are.
.PP
In Nagios check mode, exit codes are:
.TP
.B 0
Success
.TP
.B 1
Warning condition.
.TP
.B 2
Critical condition.
.TP
.B 3
Unknown.  This code is returned in the following cases: (1) the host
is not monitored by the
.B ping903
server, (2) the server replied with a HTTP code other than 200 or the
reply was otherwise non-compliant, and (3) if the server has not
yet collected enough data for that host and the command line option
.B \-N
was given.
.PP
In match mode, the program exits with code 0 if the requested host is
monitored by the server, and with code 2 if it is not.
.PP
If any error is encountered, \fBping903q\fR exits with status \fB3\fR.
.SH OPTIONS
.TP
\fB\-a\fR
Query statistics for all monitored hosts.
.TP
\fB\-f \fIFILE\fR
Read configuration from \fIFILE\fR instead of from the default
.BR /etc/ping903.conf .
.TP
.B \-h
Print a short usage summary.
.TP
\fB\-R \fIREALM\fR
Specifies authentication realm to use to authenticate the request.
This option causes the program to look up the credentials in the
.B .ping903.cred 
file at startup and to use them when forming the request as described
in \fBBasic authorization\fR above.
.TP
.B \-r
Fuzzy matching mode.  First, issue a match request looking for IP
addresses matching each \fIHOST\fR from the command line.  Then,
query statistics for each of the returned IP addresses.
.TP
.B \-V
Print program version, copyright information, and exit.
.TP
.B \-v
Turn on verbose output.
.SS Options specific for Nagios check mode
The presence of any of the three options below switches \fBping903q\fR
to Nagios check mode.  For this mode to succeed, all three options
must be specified.
.TP
\fB\-H \fIHOST\fR
Sets host name or IP address to query for.
.TP
\fB\-c \fIRTA\fB,\fIPCT\fB%\fR
Sets the critical threshold value.  \fIRTA\fR is the round-trip
average and \fIPCT\fR is the package loss percentage values.  The
critical condition is entered if either the returned round-trip
average becomes greater than or equal to \fIRTA\fR or the lost package
percentage becomes greater than or equal to \fIPCT\fR.  Note that both
parts must be present and must be valid floating-point numbers.  Note
also that the use of the percent sign is mandatory.
.TP
\fB\-w \fIRTA\fB,\fIPCT\fB%\fR
Sets the warning threshold value.  See above for the discussion of the
arguments.
.PP
Other options valid in this mode:
.TP
\fB\-N\fR
By default, hosts in initial state (i.e. for which no data has been
collected) are treated as "OK" (exit status 0).  This option changes
the default to treat them as "UNKNOWN" (exit status 3).
.TP
\fB\-p \fIPREFIX\fR
Supplies the prefix to be displayed before Nagios status string.  The
default is "PING".  The \fIPREFIX\fR string can contain the
\fB%h\fR escape sequence, which will be expanded to the name of the
host being monitored.  E.g. \fB\-p 'PING %h'\fR.
.SS Options specific for match mode
.TP
.B \-m
Switch to the host match mode.
.SH SEE ALSO
.BR ping903 (8),
.BR ping903.cred (5),
.B Nagios
<https://www.nagios.org/>.
.SH COPYRIGHT
Copyright \(co 2020\(en2025 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_.\\-]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:
