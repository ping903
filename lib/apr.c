/*
 * This is work is derived from material Copyright RSA Data Security, Inc.
 *
 * The RSA copyright statement and Licence for that original material is
 * included below. This is followed by the Apache copyright statement and
 * licence for the modifications made to that material.
 */

/* MD5C.C - RSA Data Security, Inc., MD5 message-digest algorithm
 */

/* Copyright (C) 1991-2, RSA Data Security, Inc. Created 1991. All
   rights reserved.
   License to copy and use this software is granted provided that it
   is identified as the "RSA Data Security, Inc. MD5 Message-Digest
   Algorithm" in all material mentioning or referencing this software
   or this function.
   License is also granted to make and use derivative works provided
   that such works are identified as "derived from the RSA Data
   Security, Inc. MD5 Message-Digest Algorithm" in all material
   mentioning or referencing the derived work.
   RSA Data Security, Inc. makes no representations concerning either
   the merchantability of this software or the suitability of this
   software for any particular purpose. It is provided "as is"
   without express or implied warranty of any kind.
   These notices must be retained in any copies of any part of this
   documentation and/or software.
 */

/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <config.h>
#include <sys/types.h>
#include <stdint.h>
#include <string.h>
#include "md5.h"

/* APR-specific code */

/*
 * Define the Magic String prefix that identifies a password as being
 * hashed using our algorithm.
 */
#define APR1_ID_STR "$apr1$"
#define APR1_ID_LEN (sizeof(APR1_ID_STR)-1)

/*
 * The following MD5 password encryption code was largely borrowed from
 * the FreeBSD 3.0 /usr/src/lib/libcrypt/crypt.c file, which is
 * licenced as stated at the top of this file.
 */

static void 
to64(char *s, unsigned long v, int n)
{
	static unsigned char itoa64[] =         /* 0 ... 63 => ASCII - 64 */
            "./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    	while (--n >= 0) {
        	*s++ = itoa64[v&0x3f];
        	v >>= 6;
    	}
}

#define APR_MD5_DIGESTSIZE 16

char *
apr_md5_encode(const char *pw, const char *salt, char *result, size_t nbytes)
{
    	/*
     	* Minimum size is 8 bytes for salt, plus 1 for the trailing NUL,
        * plus 4 for the '$' separators, plus the password hash itself.
        * Let's leave a goodly amount of leeway.
        */

    	char passwd[120], *p;
    	const char *sp, *ep;
    	unsigned char final[APR_MD5_DIGESTSIZE];
    	ssize_t sl, pl, i;
    	MD5_CTX ctx, ctx1;
    	unsigned long l;

    	/* 
     	* Refine the salt first.  It's possible we were given an already-hashed
    	* string as the salt argument, so extract the actual salt value from it
    	* if so.  Otherwise just use the string up to the first '$' as the salt.
     	*/
    	sp = salt;

    	/*
     	* If it starts with the magic string, then skip that.
     	*/
    	if (!strncmp(sp, APR1_ID_STR, APR1_ID_LEN)) {
        	sp += APR1_ID_LEN;
   	}

    	/*
     	* It stops at the first '$' or 8 chars, whichever comes first
     	*/
    	for (ep = sp; (*ep != '\0') && (*ep != '$') && (ep < (sp + 8)); ep++) 
        	;

    	/*
     	* Get the length of the true salt
     	*/
    	sl = ep - sp;

    	/*
     	* 'Time to make the doughnuts..'
     	*/
    	MD5Init(&ctx);
    
   	/*
     	* The password first, since that is what is most unknown
     	*/
    	MD5Update(&ctx, (unsigned char const*) pw, strlen(pw));

    	/*
     	* Then our magic string
     	*/
    	MD5Update(&ctx, (unsigned char const*)APR1_ID_STR, APR1_ID_LEN);

    	/*
     	* Then the raw salt
     	*/
    	MD5Update(&ctx, (unsigned char const*)sp, sl);

    	/*
     	* Then just as many characters of the MD5(pw, salt, pw)
     	*/
    	MD5Init(&ctx1);
    	MD5Update(&ctx1, (unsigned char const*)pw, strlen(pw));
    	MD5Update(&ctx1, (unsigned char const*)sp, sl);
    	MD5Update(&ctx1, (unsigned char const*)pw, strlen(pw));
    	MD5Final(final, &ctx1);
    	for (pl = strlen(pw); pl > 0; pl -= APR_MD5_DIGESTSIZE) {
       		MD5Update(&ctx, final,
                          (pl > APR_MD5_DIGESTSIZE) ? APR_MD5_DIGESTSIZE : pl);
    	}

    	/*
     	* Don't leave anything around in vm they could use.
     	*/
    	memset(final, 0, sizeof(final));

    	/*
     	* Then something really weird...
     	*/
    	for (i = strlen(pw); i != 0; i >>= 1) {
        	if (i & 1) 
            		MD5Update(&ctx, final, 1);
        	else
            		MD5Update(&ctx, (unsigned char const*)pw, 1);
    	}

    	/*
     	* Now make the output string.  We know our limitations, so we
     	* can use the string routines without bounds checking.
     	*/
    	strcpy(passwd, APR1_ID_STR);
    	strncat(passwd, sp, sl);
    	strcat(passwd, "$");

    	MD5Final(final, &ctx);

    	/*
    	* And now, just to make sure things don't run too fast..
     	* On a 60 Mhz Pentium this takes 34 msec, so you would
     	* need 30 seconds to build a 1000 entry dictionary...
     	*/
    	for (i = 0; i < 1000; i++) {
        	MD5Init(&ctx1);
         	/*
          	* apr_md5_final clears out ctx1.xlate at the end of each loop,
          	* so need to to set it each time through
          	*/
        	if (i & 1)
            		MD5Update(&ctx1, (unsigned char const*)pw, strlen(pw));
        	else 
            		MD5Update(&ctx1, final, APR_MD5_DIGESTSIZE);
        	if (i % 3)
            		MD5Update(&ctx1, (unsigned char const*)sp, sl);

        	if (i % 7) 
            		MD5Update(&ctx1, (unsigned char const*)pw, strlen(pw));

        	if (i & 1)
            		MD5Update(&ctx1, final, APR_MD5_DIGESTSIZE);
        	else 
            		MD5Update(&ctx1, (unsigned char const*)pw, strlen(pw));
        	MD5Final(final,&ctx1);
    	}

    	p = passwd + strlen(passwd);

    	l = (final[ 0]<<16) | (final[ 6]<<8) | final[12]; to64(p, l, 4); p += 4;
    	l = (final[ 1]<<16) | (final[ 7]<<8) | final[13]; to64(p, l, 4); p += 4;
    	l = (final[ 2]<<16) | (final[ 8]<<8) | final[14]; to64(p, l, 4); p += 4;
    	l = (final[ 3]<<16) | (final[ 9]<<8) | final[15]; to64(p, l, 4); p += 4;
    	l = (final[ 4]<<16) | (final[10]<<8) | final[ 5]; to64(p, l, 4); p += 4;
    	l =                    final[11]                ; to64(p, l, 2); p += 2;
    	*p = '\0';

    	/*
     	* Don't leave anything around in vm they could use.
     	*/
    	memset(final, 0, sizeof(final));

	i = strlen(passwd);
	if (i >= nbytes)
		i = nbytes - 1;
	memcpy(result, passwd, i);
	result[i] = 0;
	return result;
}

#ifdef STANDALONE
int
main(int argc, char **argv)
{
        unsigned char result[120];
	if (argc != 3)
		exit(1);
	apr_md5_encode(argv[1], argv[2], result, sizeof(result));
	printf("%s\n",result);
}
#endif
