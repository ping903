typedef enum {
	BASICAUTH_ALLOW,
	BASICAUTH_DENY,
	BASICAUTH_BAD_INPUT,
	BASICAUTH_CANT_OPEN
} BASICAUTH_RESULT;

BASICAUTH_RESULT basicauth(char const *file, char const *input);

int base64_encode(const unsigned char *input, size_t input_len,
		  unsigned char **output, size_t *output_len);
int base64_decode(const unsigned char *input, size_t input_len,
		  unsigned char *output, size_t output_len);
